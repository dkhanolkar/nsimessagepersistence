package au.com.spausnet.nsimessagepersistence.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.com.spausnet.nsimessagepersistence.camelcontexthandler.CamelContextHandler;

public class TestCamelLogController implements Controller{

	@Autowired
	private CamelContextHandler camelContextHandle;
		
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		/*Component component = camelContextHandle.getCamelContext().getComponent("log");
		Endpoint endpoint = component.createEndpoint("log:au.com.spausnet");
		Exchange exchange = endpoint.createExchange();
		exchange.getIn().setBody("Hello "+request.getParameter("name"));
		*/
		ProducerTemplate template = camelContextHandle.getCamelContext().createProducerTemplate();
		System.out.println("Sending body to log component");
		template.sendBody("log:au.com.spausnet" , "Hello "+request.getParameter("name"));
		return null;
	}

	public CamelContextHandler getCamelContextHandle() {
		return camelContextHandle;
	}

	public void setCamelContextHandle(CamelContextHandler camelContextHandle) {
		this.camelContextHandle = camelContextHandle;
	}

}
