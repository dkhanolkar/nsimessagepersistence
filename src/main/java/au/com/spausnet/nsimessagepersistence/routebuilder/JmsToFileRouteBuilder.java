package au.com.spausnet.nsimessagepersistence.routebuilder;

import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JmsToFileRouteBuilder extends RouteBuilder{
	
	private final Log logger = LogFactory.getLog(getClass());
	
	private MessagePersistenceProcessor processor;
	

	public MessagePersistenceProcessor getProcessor() {
		return processor;
	}


	public void setProcessor(MessagePersistenceProcessor processor) {
		this.processor = processor;
	}


	@Override
	public void configure() throws Exception {
		//from("jms:topic:tpBPM_ServiceOrder_Out?clientId=N_M_P_SO_OUT&durableSubscriptionName=N_M_P_SO_OUT").to("file://target/out/SO.txt?autoCreate=true");
		//from("jms:topic:tpBPM_ServiceOrder_Out?clientId=ESB_NSIMSGPERSISTENCE_SO&durableSubscriptionName=N_M_P_SO_OUT").to("log:au.com.spausnet");
		logger.info("Starting NSI MESSAGE PERSISTENCE ROUTES");
		from("jms:topic:tpBPM_MeterDataNotificationAndResponse_Out?clientId=ESB_NSIMSGPERSISTENCE_MDNR&durableSubscriptionName=N_M_P_MDNR_OUT").process(processor);
		
		from("jms:topic:tpBPM_ServiceOrder_Out?clientId=ESB_NSIMSGPERSISTENCE_SO&durableSubscriptionName=N_M_P_SO_OUT").process(processor);
		
		logger.info("Done creating NSI MESSAGE PERSISTENCE ROUTES");
	}


	
	
}
