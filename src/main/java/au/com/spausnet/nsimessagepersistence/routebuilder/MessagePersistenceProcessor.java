package au.com.spausnet.nsimessagepersistence.routebuilder;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import au.com.spausnet.nsimessagepersistence.dao.entity.HbmNsiMessageEntity;
import au.com.spausnet.nsimessagepersistence.dao.impl.NsiMessagePersistenceDAO;
import au.com.spausnet.nsimessagepersistence.parsers.xml.AseXMLParser;
import au.com.spausnet.nsimessagepersistence.parsers.xml.DOMAseXMLParser;

public class MessagePersistenceProcessor implements Processor{

	private NsiMessagePersistenceDAO nsiMsgPersistenceDaoImpl;
	
	private final Log logger = LogFactory.getLog(getClass());
	
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		
		String xmlBody = exchange.getIn().getBody().toString();
		logger.debug("xmlBody received from JMS Message - "+xmlBody);
		logger.debug("Starting to Parse message");
		AseXMLParser parser = new DOMAseXMLParser();
		logger.debug("Finished parsing message, now persisting");
		nsiMsgPersistenceDaoImpl.persistNsiSmf(new HbmNsiMessageEntity(parser.getMessageIDValue(xmlBody), xmlBody));
		
		/*System.out.println(xmlBody);
		System.out.println("The message id is - "+parser.getMessageIDValue(xmlBody));
		*/
	}
	
	public NsiMessagePersistenceDAO getNsiMsgPersistenceDaoImpl() {
		return nsiMsgPersistenceDaoImpl;
	}


	public void setNsiMsgPersistenceDaoImpl(NsiMessagePersistenceDAO nsiMsgPersistenceDaoImpl) {
		this.nsiMsgPersistenceDaoImpl = nsiMsgPersistenceDaoImpl;
	}
	
	

}
