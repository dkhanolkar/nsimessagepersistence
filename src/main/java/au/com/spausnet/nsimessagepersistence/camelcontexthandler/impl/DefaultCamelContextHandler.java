package au.com.spausnet.nsimessagepersistence.camelcontexthandler.impl;

import org.apache.camel.CamelContext;
import org.apache.camel.Component;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.component.log.LogComponent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import au.com.spausnet.nsimessagepersistence.camel.components.wm.WmBrokerConnectionFactoryProvider;
import au.com.spausnet.nsimessagepersistence.camelcontexthandler.CamelContextHandler;

public class DefaultCamelContextHandler implements CamelContextHandler{
	
	private final Log logger = LogFactory.getLog(getClass());
	
	private CamelContext camelContext;
	private WmBrokerConnectionFactoryProvider brokerFactory;
	

	public void addCamelComponent(String componentName , Component<?> component) {
		
		camelContext.addComponent(componentName, component);
		
	}

	public void removeCamelComponent(String componentName) {
		// TODO Auto-generated method stub
		
	}

	public CamelContext getCamelContext() {
		return camelContext;
	}

	public void setCamelContext(CamelContext camelContext) {
		this.camelContext = camelContext;
	}

	public void startCamelContext() {
		try {
			this.camelContext.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void stopCamelContext() {
		// TODO Auto-generated method stub
		try {
			this.camelContext.stop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void instantiateCamelContext() {
		// TODO Auto-generated method stub
		logger.info("Starting to Instantiating Camel Context");
		this.addCamelComponent("log" , new LogComponent());		
		this.addCamelComponent("jms" , new JmsComponent(new JmsConfiguration(brokerFactory.createConnectionFactory())));
		this.startCamelContext();
		logger.info("Finished Instantiating Camel Context");
		
		
	}

	public ProducerTemplate getProducerTemplate() {
		// TODO Auto-generated method stub
		return this.getCamelContext().createProducerTemplate();
		
	}

	public WmBrokerConnectionFactoryProvider getBrokerFactory() {
		return brokerFactory;
	}

	public void setBrokerFactory(WmBrokerConnectionFactoryProvider brokerFactory) {
		this.brokerFactory = brokerFactory;
	}
	
	
	
	

}
