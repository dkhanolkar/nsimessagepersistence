package au.com.spausnet.nsimessagepersistence.camelcontexthandler;

import org.apache.camel.CamelContext;
import org.apache.camel.Component;
import org.apache.camel.ProducerTemplate;

public interface CamelContextHandler {
	
	public void startCamelContext();//starts a camel context
	public void addCamelComponent(String componentName , Component<?> component);//adds a camel component
	public void removeCamelComponent(String componentName);//removes a camel component
	public void stopCamelContext();//stops a camel context
	public void instantiateCamelContext();//adds camel components and starts camel context
	public CamelContext getCamelContext();//gets the current camel context
	public ProducerTemplate getProducerTemplate();

}
