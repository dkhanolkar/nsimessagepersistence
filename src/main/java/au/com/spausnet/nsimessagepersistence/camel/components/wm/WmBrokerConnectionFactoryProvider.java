package au.com.spausnet.nsimessagepersistence.camel.components.wm;

import java.util.Properties;

import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class WmBrokerConnectionFactoryProvider {
	
	private WmBrokerComponent brokerComp;

	public WmBrokerComponent getBrokerComp() {
		return brokerComp;
	}

	public void setBrokerComp(WmBrokerComponent brokerComp) {
		this.brokerComp = brokerComp;
	}
	
	public ConnectionFactory createConnectionFactory(){
		
		Context ic;
		Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, brokerComp.getJmsContext());        
        ConnectionFactory connectionFactory=null;
        props.put(Context.PROVIDER_URL, brokerComp.getProviderURL());
        try {
			ic = new InitialContext(props);
			connectionFactory = (ConnectionFactory)ic.lookup(brokerComp.getLookup());
			ic.close();
        } catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connectionFactory;	
				
	}

}
