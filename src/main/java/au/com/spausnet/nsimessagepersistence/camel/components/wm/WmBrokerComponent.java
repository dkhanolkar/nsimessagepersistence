package au.com.spausnet.nsimessagepersistence.camel.components.wm;


public interface WmBrokerComponent {
	
	public String getProviderURL();
	public void setProviderURL(String providerURL);
	public String getJmsContext();
	public void setJmsContext(String jmsContext);
	public String getLookup();
	public void setLookup(String lookup);

}
