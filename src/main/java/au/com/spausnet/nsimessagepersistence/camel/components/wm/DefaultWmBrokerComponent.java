package au.com.spausnet.nsimessagepersistence.camel.components.wm;

import java.util.Properties;

import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class DefaultWmBrokerComponent implements WmBrokerComponent{
	
	private String providerURL , jmsContext , lookup;	
	
	public String getProviderURL() {
		return providerURL;
	}

	public void setProviderURL(String providerURL) {
		this.providerURL = providerURL;
	}

	public String getJmsContext() {
		return jmsContext;
	}

	public void setJmsContext(String jmsContext) {
		this.jmsContext = jmsContext;
	}

	public String getLookup() {
		return lookup;
	}

	public void setLookup(String lookup) {
		this.lookup = lookup;
	}

}
