package au.com.spausnet.nsimessagepersistence.parsers.xml;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class DOMAseXMLParser implements AseXMLParser{

	private final Log logger = LogFactory.getLog(getClass());
	
	public String getMessageIDValue(String xmlContent) {

		DocumentBuilder dbuilder;
		Document doc;
		Element rootElement;
		Element headerElement;
		Element messageIDElement=null;
		
		try {
			dbuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			//doc = dbuilder.parse(new File(xmlFilePath));
			logger.debug("XML Content to parse "+xmlContent);
			doc = dbuilder.parse(new ByteArrayInputStream(xmlContent.getBytes()));			
			rootElement = doc.getDocumentElement();//this gets us to the ase:aseXML
			headerElement = (Element)rootElement.getElementsByTagName("Header").item(0);//using ase:aseXML, get to Header
			messageIDElement = (Element)headerElement.getElementsByTagName("MessageID").item(0);//using Header, get to MessageID
			logger.debug("Message id post parsing is "+messageIDElement.getTextContent());
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		
		
		return messageIDElement.getTextContent();
	}
	

}
