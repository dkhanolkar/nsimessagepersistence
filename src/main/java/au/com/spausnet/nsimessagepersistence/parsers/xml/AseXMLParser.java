package au.com.spausnet.nsimessagepersistence.parsers.xml;

public interface AseXMLParser {
	
	public String getMessageIDValue(String xmlContent);

}
