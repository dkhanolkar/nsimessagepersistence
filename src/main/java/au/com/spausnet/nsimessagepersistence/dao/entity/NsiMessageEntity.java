package au.com.spausnet.nsimessagepersistence.dao.entity;

import java.io.Serializable;

public interface NsiMessageEntity extends Serializable{
	
	public String getMessageId();
	public String getNsiSmfXml();
	
	public void setMessageId(String messageId);
	public void setNsiSmfXml(String nsiSmfXml);

}
