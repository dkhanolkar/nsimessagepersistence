package au.com.spausnet.nsimessagepersistence.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import au.com.spausnet.nsimessagepersistence.dao.entity.NsiMessageEntity;

public interface NsiMessagePersistenceDAO {
	
	@Transactional
	public void persistNsiSmf(NsiMessageEntity entity);
	public SessionFactory getSessionFactory();

}
