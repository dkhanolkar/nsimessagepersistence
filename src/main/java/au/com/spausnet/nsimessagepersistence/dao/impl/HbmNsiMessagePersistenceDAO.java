package au.com.spausnet.nsimessagepersistence.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import au.com.spausnet.nsimessagepersistence.dao.entity.NsiMessageEntity;

public class HbmNsiMessagePersistenceDAO implements NsiMessagePersistenceDAO {

	private final Log logger = LogFactory.getLog(getClass());
	
	private SessionFactory sessionFactory;
	
	@Transactional
	public void persistNsiSmf(NsiMessageEntity entity) {
		// TODO Auto-generated method stub
		logger.debug("Entity to DB "+entity.getMessageId());
		HibernateTemplate template = new HibernateTemplate(sessionFactory);			
		template.save(entity);		
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void closeSession(){
		
		this.sessionFactory.close();
		
	}

}
