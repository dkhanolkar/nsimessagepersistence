package au.com.spausnet.nsimessagepersistence.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="NSI_SMF_MESSAGES")
public class HbmNsiMessageEntity implements NsiMessageEntity{

	static final long serialVersionUID = 618568767921274631L;
	
	private String messageId , nsiSmfXml;

	@Id
	@Column(name="MESSAGE_ID")
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	
	@Column(name="NSI_SMF_XML")
	public String getNsiSmfXml() {
		return nsiSmfXml;
	}

	public void setNsiSmfXml(String nsiSmfXml) {
		this.nsiSmfXml = nsiSmfXml;
	}
	
	public HbmNsiMessageEntity(String messageId , String nsiSmfXml){
		
		this.messageId = messageId;
		this.nsiSmfXml = nsiSmfXml;
		
	}
	
	public HbmNsiMessageEntity(){	
		
	} 
	

}
