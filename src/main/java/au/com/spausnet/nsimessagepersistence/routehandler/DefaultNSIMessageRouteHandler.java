package au.com.spausnet.nsimessagepersistence.routehandler;

import org.apache.camel.CamelContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import au.com.spausnet.nsimessagepersistence.camelcontexthandler.CamelContextHandler;
import au.com.spausnet.nsimessagepersistence.routebuilder.JmsToFileRouteBuilder;

public class DefaultNSIMessageRouteHandler implements NSIMessageRouteHandler{

	private final Log logger = LogFactory.getLog(getClass());
	
	private CamelContextHandler camelContextHandle;
	private JmsToFileRouteBuilder routeBuilder;
	
	public void setupNSIRoutes() {
		// TODO Auto-generated method stub
		CamelContext context = this.camelContextHandle.getCamelContext();
		try {
			logger.debug("Adding routes");
			context.addRoutes(this.routeBuilder);
			logger.debug("Done adding routes");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public CamelContextHandler getCamelContextHandle() {
		return camelContextHandle;
	}

	public void setCamelContextHandle(CamelContextHandler camelContextHandle) {
		this.camelContextHandle = camelContextHandle;
	}

	public JmsToFileRouteBuilder getRouteBuilder() {
		return routeBuilder;
	}

	public void setRouteBuilder(JmsToFileRouteBuilder routeBuilder) {
		this.routeBuilder = routeBuilder;
	}
	

}
