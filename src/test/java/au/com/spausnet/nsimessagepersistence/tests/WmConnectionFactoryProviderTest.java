package au.com.spausnet.nsimessagepersistence.tests;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import au.com.spausnet.nsimessagepersistence.camel.components.wm.WmBrokerConnectionFactoryProvider;


@SuppressWarnings("deprecation")
public class WmConnectionFactoryProviderTest extends AbstractDependencyInjectionSpringContextTests{
	
	private WmBrokerConnectionFactoryProvider brokerFactory;
	
	@Override
	protected String[] getConfigLocations() {
		// TODO Auto-generated method stub
		return new String[]{"classpath:/applicationContext-test.xml"};
	}
	
	
	public void testWmConnectionFactoryProvider(){
		
		
		ConnectionFactory factory = this.brokerFactory.createConnectionFactory();
		try {
			
			Connection conn = factory.createConnection();
			conn.setClientID("testclient1");
			System.out.println("JMS Major version"+conn.getMetaData().getJMSMajorVersion());
			System.out.println("JMS Provider name "+conn.getMetaData().getJMSProviderName());
			assertEquals(1 , conn.getMetaData().getJMSMajorVersion());
			assertEquals("webMethods JMS Provider" , conn.getMetaData().getJMSProviderName());
			conn.close();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	public WmBrokerConnectionFactoryProvider getBrokerFactory() {
		return brokerFactory;
	}

	public void setBrokerFactory(WmBrokerConnectionFactoryProvider brokerFactory) {
		this.brokerFactory = brokerFactory;
	}
	

}
