package au.com.spausnet.nsimessagepersistence.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import junit.framework.TestCase;
import au.com.spausnet.nsimessagepersistence.parsers.xml.DOMAseXMLParser;

public class AseXMLParserTest extends TestCase {
	
	public void testXMLParser() throws IOException{
		
		File f = new File("C:\\devendra\\Java_Apps_Workspace\\NSIMessagePersistence\\src\\test\\resources\\test.xml");
		
		BufferedReader reader = new BufferedReader(new FileReader(f));
		String xmlContent = "";
		String temp = "";
		while((temp=reader.readLine())!= null){
			
			xmlContent = xmlContent+temp;
			
		}	
		
		assertEquals("OER_MSG_20130421093234059-9105", new DOMAseXMLParser().getMessageIDValue(xmlContent));
		
	}

}
