package au.com.spausnet.nsimessagepersistence.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.camel.ProducerTemplate;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import au.com.spausnet.nsimessagepersistence.camelcontexthandler.CamelContextHandler;
import au.com.spausnet.nsimessagepersistence.dao.entity.HbmNsiMessageEntity;
import au.com.spausnet.nsimessagepersistence.dao.entity.NsiMessageEntity;
import au.com.spausnet.nsimessagepersistence.dao.impl.NsiMessagePersistenceDAO;

@SuppressWarnings("deprecation")
public class CamelNsiJmsToFileRouteTest extends AbstractDependencyInjectionSpringContextTests{
	
	private CamelContextHandler camelContextHandle;
	private NsiMessagePersistenceDAO nsiMsgPersistenceDaoImpl;
	
	
	@Override
	protected String[] getConfigLocations() {
		// TODO Auto-generated method stub
		return new String[]{"classpath:/applicationContext-test.xml"};
	}
	
	public void testNsiJmsToFileRoute() throws IOException, InterruptedException{
		
		@SuppressWarnings("rawtypes")
		ProducerTemplate template = camelContextHandle.getCamelContext().createProducerTemplate();
		System.out.println("CamelNsiJmsToFileRouteTest - Sending body to log component1");
		//template.sendBody("log:au.com.spausnet" , "Hello");	
		
		File f = new File("C:\\devendra\\Java_Apps_Workspace\\NSIMessagePersistence\\src\\test\\resources\\test.xml");
		
		BufferedReader reader = new BufferedReader(new FileReader(f));
		String xmlContent = "";
		String temp = "";
		while((temp=reader.readLine())!= null){
			
			xmlContent = xmlContent+temp;
			
		}
		reader.close();
		
		template.sendBody("jms:topic:tpBPM_ServiceOrder_Out" , xmlContent);
		Thread.sleep(5000L);
				
		SessionFactory sessionFactory = this.nsiMsgPersistenceDaoImpl.getSessionFactory();
		HibernateTemplate hbmTemplate = new HibernateTemplate(sessionFactory);
		
		assertEquals(1 , hbmTemplate.find("from HbmNsiMessageEntity aa where aa.messageId='OER_MSG_20130421093234059-9105'").size());
		

		
	}

	public CamelContextHandler getCamelContextHandle() {
		return camelContextHandle;
	}

	public void setCamelContextHandle(CamelContextHandler camelContextHandle) {
		this.camelContextHandle = camelContextHandle;
	}

	public NsiMessagePersistenceDAO getNsiMsgPersistenceDaoImpl() {
		return nsiMsgPersistenceDaoImpl;
	}

	public void setNsiMsgPersistenceDaoImpl(NsiMessagePersistenceDAO nsiMsgPersistenceDaoImpl) {
		this.nsiMsgPersistenceDaoImpl = nsiMsgPersistenceDaoImpl;
	}

}
