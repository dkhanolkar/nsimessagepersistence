package au.com.spausnet.nsimessagepersistence.tests;

import org.apache.camel.ProducerTemplate;
import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import au.com.spausnet.nsimessagepersistence.camelcontexthandler.CamelContextHandler;

@SuppressWarnings("deprecation")
public class CamelContextHandlerTest extends
		AbstractDependencyInjectionSpringContextTests {
	
	private CamelContextHandler camelContextHandle;
	
	@Override
	protected String[] getConfigLocations() {
		// TODO Auto-generated method stub
		return new String[]{"classpath:/applicationContext-test.xml"};
	}
	
	public void testCamelContextHandle() throws InterruptedException{
		
		@SuppressWarnings("rawtypes")
		ProducerTemplate template = camelContextHandle.getCamelContext().createProducerTemplate();
		System.out.println("Sending body to log component1");
		//template.sendBody("log:au.com.spausnet" , "Hello");	
		template.sendBody("jms:topic:tpBPM_MeterDataNotificationAndResponse_Out" , "HELLO WORLD4");		
		
	}

	public CamelContextHandler getCamelContextHandle() {
		return camelContextHandle;
	}

	public void setCamelContextHandle(CamelContextHandler camelContextHandle) {
		this.camelContextHandle = camelContextHandle;
	}

}
