package au.com.spausnet.nsimessagepersistence.tests;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import au.com.spausnet.nsimessagepersistence.dao.entity.HbmNsiMessageEntity;
import au.com.spausnet.nsimessagepersistence.dao.entity.NsiMessageEntity;
import au.com.spausnet.nsimessagepersistence.dao.impl.NsiMessagePersistenceDAO;

public class NsiMessagePersistenceDAOTest extends AbstractDependencyInjectionSpringContextTests {
	
	private NsiMessagePersistenceDAO nsiMsgPersistenceDaoImpl;

	public NsiMessagePersistenceDAO getNsiMsgPersistenceDaoImpl() {
		return nsiMsgPersistenceDaoImpl;
	}

	public void setNsiMsgPersistenceDaoImpl(NsiMessagePersistenceDAO nsiMsgPersistenceDaoImpl) {
		this.nsiMsgPersistenceDaoImpl = nsiMsgPersistenceDaoImpl;
	}
	
	@Override
	protected String[] getConfigLocations() {
		// TODO Auto-generated method stub
		return new String[]{"classpath:/applicationContext-test.xml"};
	}
	
	public void testNsiMessagePersistenceDAO(){
		
		NsiMessageEntity entity = new HbmNsiMessageEntity("123456789" , "some arbitary xml");
		this.nsiMsgPersistenceDaoImpl.persistNsiSmf(entity);
		
		SessionFactory sessionFactory = this.nsiMsgPersistenceDaoImpl.getSessionFactory();
		HibernateTemplate template = new HibernateTemplate(sessionFactory);
		
		assertEquals(1 , template.find("from HbmNsiMessageEntity aa where aa.messageId='123456789'").size());
		
	}
	

}
